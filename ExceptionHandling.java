import java.util.InputMismatchException;
import java.util.Scanner;

public class ExceptionHandling {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Program Pembagian Bilangan");

        boolean validInput = false;
        do {
            //edit here
            try {
                System.out.print("Masukkan pembilang: "); int pembilang = scanner.nextInt();
                System.out.print("Masukkan penyebut: "); int penyebut = scanner.nextInt();
                int hasil = pembagian(pembilang,penyebut);
                System.out.println("Hasil pembagian bilangan: " + hasil);
                validInput = true;
            } catch (ArithmeticException e){
                System.out.println("Penyebut tidak boleh bernilai 0");
                scanner.nextLine();

            } catch (InputMismatchException e) {
                System.out.println("Input yang anda masukkan bukan bilangan bulat");
                scanner.nextLine();
            }
        } while (!validInput);

        scanner.close();
    }

    public static int pembagian(int pembilang, int penyebut) {
        //add exception apabila penyebut bernilai 0
        if (penyebut == 0){
            throw new ArithmeticException("Penyebut tidak boleh bernilai 0");
        }
        return pembilang / penyebut;
    }
}


